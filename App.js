/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import Bluetooth from "./src/components/smart/Bluetooth";
import stores from "./src/stores";
import {Provider} from "mobx-react";
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider {...stores}>
        <Bluetooth/>
      </Provider>
    );
  }
}

